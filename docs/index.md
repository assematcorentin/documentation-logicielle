# Configuration d'un environnement de travail

## Ordre d'execution de la configuration :

* [Docker](/en/amelioration/Docker/Docker/)
* [Jenkins](/en/amelioration/Jenkins/Jenkins/)
* [SonarQube](/en/amelioration/SonarQube/SonarQube/)
* [Gitlab](/en/amelioration/Git/Git/)