# Configuration matérielle minimale

* 1 CPU
* 1 GO de RAM
* 5 Go d'espace disque


# Configuration matérielle recommandée pour une petite équipe

* 2 CPU
* 2 Go de RAM
* 10 Go d'espace disque


# Logiciels requis

* Docker
* Navigateur Web

# Execution du Dockerfile 

* Executer le script `docker-compose.yml` depuis un terminal de commande avec la commande suivante :
`docker-compose up -d`

# Accès au site
* Accédez ensuite à l'URL : [http://localhost:80](http://localhost:80)