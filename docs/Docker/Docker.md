# Configuration requise 

* Windows 10 64 bits: Pro, Enterprise ou Education (Build 15063 ou version ultérieure).
* Hyper-V et conteneurs Les fonctionnalités Windows doivent être activées.


# Prérequis matériels pour exécuter avec succès Client Hyper-V sur Windows 10

* Processeur 64 bits avec traduction d'adresse de deuxième niveau (SLAT)
* 4 Go de RAM système
* La prise en charge de la virtualisation matérielle au niveau du BIOS doit être activée dans les paramètres du BIOS. Pour plus d'informations, voir Virtualisation .


# Déroulement de l'installation :

* Télécharger [docker](https://docs.docker.com/install/)
* Executez le fichier téléchargé et suivez les instructions afin d'installer docker


# Autre

Si vous ne possédez pas Windows 10 Pro, vous pouvez installer [docker toolbox](https://docs.docker.com/toolbox/toolbox_install_windows/)