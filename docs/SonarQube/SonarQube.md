# Configuration matérielle minimale

* 1 GO de RAM
* 10 Go d'espace disque


# Configuration matérielle recommandée

* 2 Go de RAM
* 15 Go d'espace disque


# Logiciels requis:
* Navigateur Web

# Installation par Zip 
* Installer le zip [du site officiel de SonarQube](https://www.sonarqube.org/downloads/) dans **C:\sonarqube** ou **/opt/sonarqube**.
Lancer la commande suivante dans un terminal :
* Pour windows : `C:\sonarqube\bin\windows-x86-xx\StartSonar.bat`
* Pour un autre OS : `/opt/sonarqube/bin/[OS]/sonar.sh console`

# Installation avec Docker
Si vous possédez Docker, lancez le script **SonarQube.bat** ou la commande qu'il contient `docker run -d -p 9000:9000 sonarqube`.

# Accès au site
Accédez ensuite à l'URL : [http://localhost:9000](http://localhost:9000) (login=admin, password=admin).
Cliquez sur **Create new project** pour initialisez un projet.
